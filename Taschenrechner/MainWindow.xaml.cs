﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Taschenrechner
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.Current.Shutdown();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            // Nimm die Zahlen aus den oberen Textboxen
            Double x=System.Convert.ToDouble(TextBoxOp1.Text);
            Double y = System.Convert.ToDouble(TextBoxOp2.Text);
            // Nimm das angewählte Operationszeichen
            Char c = ComboBoxOperation.Text[0];
            // Schicke das an Rechner::Rechne
            Rechnen.Rechner r = new Rechnen.Rechner();
            Double z = r.Rechne(x, y, c);
            // Schreib das Ergebnis in die Textbox darunter
            TextBoxErgebnis.Text = z.ToString();
        }
    }
}
