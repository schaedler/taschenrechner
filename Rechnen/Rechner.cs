﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rechnen
{
    public class Rechner
    {
        public Double Rechne(Double a, Double b, Char op)
        {
            Double erg;
            switch (op)
            {
                case '+': erg = a + b; break;
                case '-': erg = a - b; break;
                case '*': erg = a * b; break;
                case '/': erg = a / b; break;
                default: erg = 0.0; break;
            }
            return erg;
        }
    }
}
